# Gulp-starter

Kit de démarrage pour la création de tâches automatisées avec l'outil Gulp.

Le kit est compatible avec *vscode*

## Modules disponibles

-  gulp-clean-css
-  gulp-concat
-  gulp-rename
-  gulp-sourcemaps
-  gulp-uglify

## Tâches disponibles

-  minifyCssFromSrc : optimisation d'un fichier css,
-  uglifyJsFromSrc : optimisation d'un fichier js,
-  default : lance les tâches *minifyCssFromSrc* & *uglifyJsFromSrc*,
-  watch : intercepte les modifications sur les fichiers css & js et lance les tâches par défaut.

## Commentaires

Le module pour optimiser javascript (*gulp-uglify*) a été modifié par *gulp-uglify-es*