/*
 * Automatisation des tâches répétitives.
 *
 * Commandes :
 * - minifyCssFromSrc - minification des fichiers css contenu dans src/css et
 *    copie dans webroot/css
 * - uglifyJsFromSrc - optimisation des fichiers javascript contenu dans src/js
 *    et copie dans webroot/js
 * - watchFiles - lance automatiquement plusieurs tâches à la suite
 * 
 * La tâche par défaut consiste à traiter les ficheirs css et js.
 * 
 * @const gulp
 * @const rename
 * @const sourcemaps
 * @const cleanCss
 */
const gulp = require('gulp'),
    rename = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps'),
    cleanCss = require('gulp-clean-css'),
    uglify = require('gulp-uglify-es').default;

/**
 * Optimisation des fichiers css
 */
function minifyCssFromSrc() {
    return gulp.src('src/css/*.css')
        .pipe(sourcemaps.init())
        .pipe(rename(function(path) {
            path.basename += ".min";
        }))
        .pipe(cleanCss())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('webroot/css'));
}

/**
 * Optimisation des fichiers javascript
 */
function uglifyJsFromSrc() {
    return gulp.src('src/js/*.js')
        .pipe(sourcemaps.init())
        .pipe(rename(function(path) {
            path.basename += ".min";
        }))
        .pipe(uglify())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('webroot/js'));
}

/**
 * Observe les modifications
 */
function watchFiles() {
    gulp.watch('src/css/*.css', gulp.parallel(minifyCssFromSrc))
    gulp.watch('src/js/*.js', gulp.parallel(uglifyJsFromSrc))
}

const watching = gulp.parallel(watchFiles);

exports.default = gulp.parallel(minifyCssFromSrc, uglifyJsFromSrc);
exports.watch = watching;
