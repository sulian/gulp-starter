# Changelog
Tous les changements notables sur ce projet seront documentés dans ce fichier.

Le format est basé sur [Keep a Changelog](http://keepachangelog.com/fr/1.0.0/)
et suis les recommandations du [Versionnage Sémantique](http://semver.org/lang/fr/spec/v2.0.0.html).

## [Non publié]

### Ajouté

### Modifié

---

## 1.0.0

### Ajouté

- configuration de gulp
- tâche *minifyCssFromSrc*
- tâche *uglifyJsFromSrc*
