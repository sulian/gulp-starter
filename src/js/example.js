"use strict"

// test en ES5
var showLog = function() {
    console.log("Bonjour depuis Gulp via ES5");
}

// test ES6
var showLogAdvanced = () => console.log("Bonjour depuis Gulp via ES6");

showLog();
showLogAdvanced();